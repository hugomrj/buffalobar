
function FacturaVentaDetalle(){
    
   this.tipo = "facturaventadetalle";   
   this.recurso = "facturaventadetalles";   
   this.value = 0;
   this.from_descrip = "";
   this.json_descrip = ['', ''];
   
   this.dom="";
   this.carpeta=  "/comercial";   
   
   this.titulosin = "facturra venta detalle";
   this.tituloplu = "facturra venta detalle";      
    
   this.campoid=  'venta_detalle';
    
   this.tablacampos =  [ 'descripcion', 'descripcion', 'descripcion', 
                                    'descripcion', 'descripcion', 'descripcion' ];   
      
   
   this.tablaformat =  ['N', 'C', 'N',
                                    'N', 'N' , 'N'
                                ];               
      
   this.tbody_id = "facturaventadetalle-tb";
      
   //this.botones_lista = [ this.lista_new] ;
   
   this.botones_form = "facturaventadetalle-acciones";   

   this.parent = null;  
   
}



FacturaVentaDetalle.prototype.form_validar = function() {     
    
    var facturaventadetalle_cantidad = document.getElementById('facturaventadetalle_cantidad');        
    if (parseInt(NumQP(facturaventadetalle_cantidad.value)) <= 0 )         
    {
        msg.error.mostrar("Valor en cantidad incorrecta");    
        
        facturaventadetalle_cantidad.focus();
        facturaventadetalle_cantidad.select(); 
        return false;
    }       
        
    
    
    
    var facturaventadetalle_precio = document.getElementById('facturaventadetalle_precio');        
    if (parseInt(NumQP(facturaventadetalle_precio.value)) <= 0 )         
    {
        msg.error.mostrar("Valor en precio incorrecta");    
        
        facturaventadetalle_precio.focus();
        facturaventadetalle_precio.select(); 
        return false;
    }       
        
    
  
    
    
    var producto_descripcion = document.getElementById('producto_descripcion');       
    if (producto_descripcion.value.toString().trim().length == 0){
        msg.error.mostrar("Falta producto");    
        return false;
    }
    
        
    
      
    
    
    return true;
};


FacturaVentaDetalle.prototype.form_ini = function() {        
    
    var facturaventadetalle_cantidad = document.getElementById('facturaventadetalle_cantidad');          
    facturaventadetalle_cantidad.onblur  = function() {                
        facturaventadetalle_cantidad.value  = fmtNum(facturaventadetalle_cantidad.value);      
        fx_subtotal();
        
    };     
    facturaventadetalle_cantidad.onblur();        
        
        
        
    var facturaventadetalle_precio = document.getElementById('facturaventadetalle_precio');          
    facturaventadetalle_precio.onblur  = function() {                
        facturaventadetalle_precio.value  = fmtNum(facturaventadetalle_precio.value);                 
        fx_subtotal();        
    };     
    facturaventadetalle_precio.onblur();                
        
        
        
        
    var facturaventadetalle_sub_total = document.getElementById('facturaventadetalle_sub_total');          
    
    facturaventadetalle_sub_total.disabled =  true;
    
    facturaventadetalle_sub_total.onblur  = function() {                
        facturaventadetalle_sub_total.value  = fmtNum(facturaventadetalle_sub_total.value);
    };     
    facturaventadetalle_sub_total.onblur();          
    
        
    
    var facturaventadetalle_precio = document.getElementById('facturaventadetalle_precio');          
    facturaventadetalle_precio.disabled =  true;    
    
};



FacturaVentaDetalle.prototype.new = function( obj ) {                
    
        var fvd_acc = document.getElementById(  "facturaventadetalle_acciones_lista"  );
    
        boton.ini(obj);
        boton.class_a = "botonB";
        boton.blabels = ['Agregar'];        
        fvd_acc.innerHTML = boton.get_botton_base();
        
        
        var btfvd_agregar = document.getElementById(  "btn_facturaventadetalle_agregar"  );        
        btfvd_agregar.addEventListener('click',
            function(event) {    
                
                                                
                var fvd = new FacturaVentaDetalle();

                ajax.url = html.url.absolute()+'/comercial/facturaventadetalle/htmf/form.html'; 
                ajax.metodo = "GET";  
                modal.ancho = 750;
                fvd.venactiva = modal.ventana.mostrar("fvd");           
               
                
                boton.ini(fvd);
                document.getElementById( fvd.tipo +'-acciones' ).innerHTML 
                        =  boton.basicform.get_botton_okcan();   

                obj.form_ini();

                facturaventadetalle_agregar ( fvd ) ;
                
                
                var facturaventadetalle_producto = document.getElementById( 'facturaventadetalle_producto' );
                facturaventadetalle_producto.focus();
                facturaventadetalle_producto.select();  
                
                
                
            },
            false
        );    
        
        
        

    
};





function Cliente(){
    
   this.tipo = "cliente";   
   this.recurso = "clientes";   
   this.value = 0;
   this.from_descrip = "";
   this.json_descrip = ['', ''];
   
   this.dom="";
   this.carpeta=  "/comercial";   
   
   this.titulosin = "Cliente";
   this.tituloplu = "Clientes";      
    
   this.campoid=  'cliente';
   this.tablacampos =  ['cliente', 'iden_doc.descripcion',
                                'numeroidencliente', 'nombre', 
                                'telefono', 'direccion'];   
                            
   this.etiquetas =  ['Cliente', 'Iden Doc',
                                'Identificador', 'Nombre', 
                                'Telefono', 'Direccion'];                               
   
   /*
   this.tablaformat =  ['C', 'N', 'N', 
                                'R', 'R', 
                                'D', 'D' ];   
   */
      
   this.tbody_id = "cliente-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "cliente-acciones";   
      
   this.tabs =  [];
   this.parent = null;
   
        
   this.combobox = {"iden_doc":{
           "value":"iden_doc",
           "inner":"descripcion"}};
    
   
}





Cliente.prototype.lista_new = function( obj  ) {                
   
    reflex.form_new( obj );    
//    reflex.datos.combo(obj);    
    reflex.acciones.button_add( obj );                     

};



Cliente.prototype.form_validar = function() {   
    
    
    var cliente_iden_numero = document.getElementById('cliente_iden_numero');    
    if (cliente_iden_numero.value == "")         
    {
        msg.error.mostrar("Campo de identificador vacio");           
        cliente_iden_numero.focus();
        cliente_iden_numero.select();        
        return false;
    }       
        

    var cliente_nombre = document.getElementById('cliente_nombre');    
    if (cliente_nombre.value == "")         
    {
        msg.error.mostrar("Campo de nombre vacio");           
        cliente_nombre.focus();
        cliente_nombre.select();        
        return false;
    }       
    
    
    
    return true;
};





Cliente.prototype.form_ini = function() {    
    
    
    var cliente_iden_doc = document.getElementById('cliente_iden_doc');
    cliente_iden_doc.onchange = function() {
        
        var cliente_iden_digito = document.getElementById('cliente_iden_digito');
                
        if (cliente_iden_doc.value.toString().trim() == "C"){            
            cliente_iden_digito.type = 'hidden';
            cliente_iden_digito.value  = '0';
        }
        else{
            if (cliente_iden_doc.value.toString().trim() == "R"){            
                cliente_iden_digito.type = 'text';
            }
        }
    };   
    cliente_iden_doc.onchange();
    

    var cliente_iden_numero = document.getElementById('cliente_iden_numero');        
    cliente_iden_numero.onkeypress = function(evt) {            
        return isNumberKey(evt);
    };      


    
};




Cliente.prototype.carga_combos = function( obj  ) {                
    
    // cargar combo
   
        var cliente_iden_doc = document.getElementById("cliente_iden_doc");
        var idedovalue = cliente_iden_doc.value;
   
        ajax.url = html.url.absolute()+'/api/identificadorocumento/all' ;
        ajax.metodo = "GET";   
        idoc_json = ajax.private.json();               
        var oJson = JSON.parse( idoc_json ) ;
        
        for( x=0; x < oJson.length; x++ ) {
            
            var jsonvalue = (oJson[x]['iden_doc'] );            
            
            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = oJson[x]['descripcion'];                        
                document.getElementById(   "cliente_iden_doc" ).appendChild(opt);                     
            }
            
        }

            

};






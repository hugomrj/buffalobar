
SELECT  
	factura_ventas.*

FROM  
  aplicacion.factura_ventas,  
  aplicacion.clientes,  
  aplicacion.formas_pagos  
  
WHERE 
  clientes.cliente = factura_ventas.cliente 
  and formas_pagos.forma_pago = factura_ventas.forma_pago 
  and  
(    

cast(factura_numero as text) ilike '%v0%'  
or cast(timbrado_numero as text) ilike '%v0%'  

or clientes.nombre ilike '%v0%'  
or clientes.apellido ilike '%v0%'  
or formas_pagos.descripcion ilike '%v0%'  
 )   


order by factura desc

  
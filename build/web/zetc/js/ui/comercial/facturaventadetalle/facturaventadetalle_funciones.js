
   
function facturaventadetalle_coleccion ( obj ) {    
    
        reflex.recurso = obj.recurso;        
        reflex.data = "";    
            
        ajax.url = html.url.absolute()+'/'+ obj.carpeta + '/' + obj.tipo + '/htmf/coleccion.html';  

        ajax.metodo = "GET";        
        document.getElementById(  obj.dom ).innerHTML =  ajax.public.html();             
        
        
        /*
        var ftr1 = document.getElementById( "ftr1" );
        ftr1.style.display = "none";
            
        var ftr3 = document.getElementById( "ftr3" );
        ftr3.style.display = "none";            
        */
}




    
function facturaventadetalle_coleccion_llenar ( obj, json  ) {   

        tabla.ini(obj);

        var oJson = JSON.parse( json ) ;    
                
                
       
        var contid = 0;
        var monto_total = 0;
        
        var html = "";                 
        for(x=0; x < oJson.length; x++) {     
            
    
            if (parseInt(oJson[x]['venta_detalle']) == 0){
                contid ++;
            }
            else{
                contid = parseInt(oJson[x]['venta_detalle']) ;
            }
            
                html += "<tr data-linea_id=\""+
                    contid
                    +"\">";                  
        
                html += "<td>";                                      
                html += fmtNum( oJson[x]['cantidad'] );            
                html += "</td>";
                
                html += "<td colspan=\"6\" >";                                      
                html += oJson[x]['descripcion'];     
                html += "</td>";                
                
                html += "<td>";                                      
                html += oJson[x]['precio'];     
                html += "</td>";                                
                
                html += "<td>";                                      
                html += oJson[x]['gravada0'];     
                html += "</td>";                                

                html += "<td>";                                      
                html += oJson[x]['gravada5'];     
                html += "</td>";              

                html += "<td>";                                      
                html += oJson[x]['gravada10'];     
                html += "</td>";                      
                
            html += "</tr>";    
            
            
            monto_total = monto_total + parseInt(oJson[x]['sub_total']) ;
            
        }


    document.getElementById( tabla.tbody_id ).innerHTML = html;  
    
    tabla.formato(obj);
    
    var f_monto_total = document.getElementById( "f_monto_total" );  
    f_monto_total.innerHTML  = fmtNum(  monto_total  ); 
    f_monto_total.style = "text-align: right";

    
    
}


function facturaventadetalle_coleccion_local ( obj  ) {  
    
    var json = localStorage.getItem('fvdls');
    
    json =  "[" + json + "]";
    facturaventadetalle_coleccion_llenar ( obj, json  ) ;
}




function facturaventadetalle_registro_local ( obj,  id ) {    
                                                
            //var fvd = new FacturaVentaDetalle();
                
            ajax.url = html.url.absolute()+'/comercial/facturaventadetalle/htmf/form.html'; 
            ajax.metodo = "GET";  
            modal.ancho = 600;
            obj.venactiva = modal.ventana.mostrar("fvd");             



    
    
            var porcentaje4 = document.getElementById('porcentaje4');
            
            var nodo = document.createElement("input");
            nodo.name = "porcentaje";
//            nodo.id = "facturaventadetalle_porcentaje";            
            porcentaje4.appendChild(nodo);


            var  json = localStorage.getItem('fvdls');    
            json =  "[" + json + "]";            
            
            
            facturaventadetalle_form_llenar ( json, id-1 ) ;
            obj.form_ini();
            form.disabled(false);    
            
                
            
        boton.objeto = ""+obj.tipo;
        document.getElementById( obj.tipo +'-acciones' ).innerHTML = boton.basicform.get_botton_del();
        
        facturaventadetalle_deletedit_acciones (  obj, id-1 );
        
        
}




function facturaventadetalle_form_llenar ( json, id ) {    
    
            var oJson = JSON.parse(json) ;     
            
            var facturaventadetalle_cantidad = document.getElementById('facturaventadetalle_cantidad');
            facturaventadetalle_cantidad.value = oJson[id].cantidad;

            var facturaventadetalle_descripcion = document.getElementById('facturaventadetalle_descripcion');
            facturaventadetalle_descripcion.value = oJson[id].descripcion;


            var facturaventadetalle_precio = document.getElementById('facturaventadetalle_precio');
            facturaventadetalle_precio.value = oJson[id].precio_unitario;


            var facturaventadetalle_sub_total = document.getElementById('facturaventadetalle_sub_total');
            facturaventadetalle_sub_total.value = oJson[id].sub_total;
         
            

}









function facturaventadetalle_agregar ( obj ) {    



    
        var facturaventadetalle_producto = document.getElementById('facturaventadetalle_producto');
        facturaventadetalle_producto.addEventListener('blur',
            function(event) {     
                
                    facturaventadetalle_producto.value = NumQP(fmtNum( facturaventadetalle_producto.value ));   
                    
                    facturaventadetalle_producto_descripcion(facturaventadetalle_producto.value);      
                    fx_subtotal();
            },
            false
        );        
        



    
        var ico_more_producto = document.getElementById('ico-more-producto');
        ico_more_producto.addEventListener('click',
            function(event) {                                   
                
                var prod = new Producto();                                                         
                prod.acctionresul = function(id) {     

                    facturaventadetalle_producto_descripcion(id);       
                    fx_subtotal();

                    var facturaventadetalle_cantidad = document.getElementById('facturaventadetalle_cantidad');          
                    facturaventadetalle_cantidad.focus();
                    facturaventadetalle_cantidad.select();  
                        
                };
                busqueda.modal.objeto( prod );
                
            },
            false
        );              
            
            
            
            



                        
        var btn_facturaventadetalle_aceptar = document.getElementById('btn_facturaventadetalle_aceptar');
        btn_facturaventadetalle_aceptar.addEventListener('click',
            function(event) {          
                         
                
                    if ( obj.form_validar())
                    {
                        
                        var sub_total = document.getElementById('facturaventadetalle_sub_total'); 
                        var imp10 = document.getElementById('facturaventadetalle_gravada10');
                        var imp5 = document.getElementById('facturaventadetalle_gravada5');
                        var imp0 = document.getElementById('facturaventadetalle_gravada0');                        
                        //var porcentaje = document.getElementById('facturaventadetalle_porcentaje'); 
                        var impuesto = document.getElementById('facturaventadetalle_impuesto'); 
                        
                        //document.getElementById('facturaventadetalle_impuesto_porcentaje').value = porcentaje.value; 


var impuesto = 10;

//                        if ( impuesto.value == 10 ){
                        if ( impuesto == 10 ){
                            imp10.value =  NumQP(sub_total.value); 
                            imp5.value =  "0"; 
                            imp0.value =  "0"; 
                            
                        }
/*                        
                        else{
                            if ( impuesto.value == 5 ){                                
                                imp10.value =  "0";
                                imp5.value =  NumQP(sub_total.value); 
                                imp0.value =  "0";          
                            }
                            else{                            
                                if ( impuesto.value == 0 ){
                                    imp10.value =  "0";
                                    imp5.value =  "0";
                                    imp0.value =  NumQP(sub_total.value); 
                                }                            
                            }
                        }
 */                           
                           
                        imp10.value =  NumQP(sub_total.value); 
                        imp5.value =  "0"; 
                        imp0.value =  "0";                            


                        form.name = "form_"+obj.tipo;
                        var jsondet = form.datos.getjson();          
              
                        
                        var fvdls = localStorage.getItem('fvdls');
                        
/*                        
alert(fvdls[0] );                                                
alert(typeof fvdls );                        

fvdls = Array.from(fvdls);
alert(typeof fvdls );

                        fvdls.push(jsondet);                        
   */                     
                        

                        if (fvdls == ''){
                            fvdls = jsondet;
                        }
                        else{                            
                            fvdls = fvdls + ", " + jsondet;
                        }

                        
                        
                        localStorage.setItem('fvdls', fvdls);                          
                        
                        modal.ventana.cerrar(obj.venactiva);                            
                        
                        facturaventadetalle_coleccion_local ( obj );                        
                        
                        tabla.lista_registro(obj, facturaventadetalle_registro_local );    
                         
                }   
                         
            },
            false
        );        
    
    
                        
                        
    
        var btn_facturaventadetalle_cancelar = document.getElementById('btn_facturaventadetalle_cancelar');
        btn_facturaventadetalle_cancelar.addEventListener('click',
            function(event) {                                   
                modal.ventana.cerrar(obj.venactiva);    
            },
            false
        );        
    
    
};



function fx_subtotal () {    
           
    
    var cantidad = document.getElementById('facturaventadetalle_cantidad');    
    var precio = document.getElementById('facturaventadetalle_precio');
    
    
    var subtotal = parseInt(NumQP(cantidad.value)) * parseInt(NumQP(precio.value));

    document.getElementById('facturaventadetalle_sub_total').value
            = fmtNum(subtotal);
    

}




function facturaventadetalle_deletedit_acciones (  obj, id ) {    

    
        var btn_facturaventadetalle_eliminar = document.getElementById('btn_facturaventadetalle_eliminar');
        btn_facturaventadetalle_eliminar.addEventListener('click',
            function(event) {                                   

                
                borrar_facturaventadetalle_local(id);
                modal.ventana.cerrar(obj.venactiva);                            

                facturaventadetalle_coleccion_local ( obj );                                                
                tabla.lista_registro(obj, facturaventadetalle_registro_local );  

            },
            false
        );          
    
    
    
    
        var btn_facturaventadetalle_cancelar = document.getElementById('btn_facturaventadetalle_cancelar');
        btn_facturaventadetalle_cancelar.addEventListener('click',
            function(event) {                                   

                modal.ventana.cerrar(obj.venactiva);    
            },
            false
        );              
    
}





function borrar_facturaventadetalle_local (  id ) {    


        var  json = localStorage.getItem('fvdls');    
        json =  "[" + json + "]";              

        var fvdls =  "";                      
        var oJson = JSON.parse(json) ;     
        

        for(i in oJson){            
            
            let jsondet = oJson[i];
            
            if (i != id){
                if (fvdls == ''){
                    fvdls = JSON.stringify( jsondet );
                }
                else{                            
                    fvdls = fvdls + ", " + JSON.stringify( jsondet );
                }
            }
        }
        
        //fvdls = JSON.stringify( fvdls ) ;             
        localStorage.setItem('fvdls', fvdls);                          
         
    
}





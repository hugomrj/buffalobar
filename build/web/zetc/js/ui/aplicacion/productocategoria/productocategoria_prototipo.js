

function ProductoCategoria(){
    
   this.tipo = "productocategoria";   
   this.recurso = "productoscategorias";   
   this.value = 0;
   this.from_descrip = "descripcion";
   this.json_descrip = "descripcion";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
      
   
   
   this.titulosin = "Categoria de producto"
   this.tituloplu = "Categorias de productos"   
      
   this.campoid=  'categoria';
   this.tablacampos =  ['categoria', 'descripcion'];
   this.etiquetas =  ['Categoria', 'Descripcion'];
   
   this.tbody_id = "categoriaproducto-tb";
      
   this.botones_lista = [ this.new ] ;
   this.botones_form = "categoriaproducto-acciones";   
   
   this.parent = null;
   
}







ProductoCategoria.prototype.new = function( obj  ) {                

    reflex.form(obj);
    reflex.acciones.button_add(obj);          
    

    
};






ProductoCategoria.prototype.form_validar = function() {    
    return true;
};





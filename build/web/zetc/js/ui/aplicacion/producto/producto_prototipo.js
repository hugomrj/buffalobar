

function Producto(){
    
   this.tipo = "producto";   
   this.recurso = "productos";   
   this.value = 0;
   this.form_descrip = "producto_nombre";
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
      
   
   
   this.titulosin = "Producto"
   this.tituloplu = "Productos"   
      
   
   this.campoid=  'producto';
   this.tablacampos =  ['codigo', 'nombre',  'categoria.descripcion', 'precio_venta' ];
   
    this.etiquetas =  ['Codigo', 'Nombre', 'Categoria', 'Precio venta' ];                                  
    
    
    this.tablaformat = ['C', 'C', 'C', 'N' ];                                  
   
   this.tbody_id = "vehiculo-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "vehiculo-acciones";   
      
   
   this.parent = null;
   
   
    this.combobox = {"categoria":{
         "value":"categoria",
         "inner":"descripcion"}};   
   
}





Producto.prototype.lista_new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add(obj);        
    
};





Producto.prototype.form_ini = function() {    
    
    var producto_precio_venta = document.getElementById('producto_precio_venta');          
    producto_precio_venta.onblur  = function() {                
        producto_precio_venta.value  = fmtNum(producto_precio_venta.value);
    };     
    producto_precio_venta.onblur();          
        
    
};



Producto.prototype.form_validar = function() {    
    return true;
};







Producto.prototype.carga_combos = function( obj  ) {                
   
        var producto_categoria = document.getElementById("producto_categoria");
        var idedovalue = producto_categoria.value;
   
        ajax.url = html.url.absolute()+'/api/productoscategorias/all' ;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();               
                
        var oJson = JSON.parse( datajson ) ;
        
        for( x=0; x < oJson.length; x++ ) {
            
            var jsonvalue = (oJson[x]['categoria'] );            
            
            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = oJson[x]['descripcion'];                        
                producto_categoria.appendChild(opt);                     
            }
            
        }
            

};









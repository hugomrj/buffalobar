/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.REST;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author hugo
 */


@javax.ws.rs.ApplicationPath("api")
public class ApplicationVersion1 extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {

        resources.add(py.com.base.aplicacion.aperturadiaria.AperturaDiariaWS.class);
        resources.add(py.com.base.aplicacion.aperturadiariadetalle.AperturaDiariaDetalleWS.class);
        resources.add(py.com.base.aplicacion.identificador_documento.IdentificadorDocumentoWS.class);
        resources.add(py.com.base.aplicacion.producto.ProductoWS.class);
        resources.add(py.com.base.aplicacion.productocategoria.ProductoCategoriaWS.class);
        resources.add(py.com.base.comercial.cliente.ClienteWS.class);
        resources.add(py.com.base.comercial.cliente.formapago.FormaPagoWS.class);
        resources.add(py.com.base.comercial.facturaventa.FacturaVentaWS.class);
        resources.add(py.com.base.sistema.rol.RolWS.class);
        resources.add(py.com.base.sistema.rol_selector.RolSelectorWS.class);
        resources.add(py.com.base.sistema.selector.SelectorWS.class);
        resources.add(py.com.base.sistema.usuario.UsuarioWS.class);
        resources.add(py.com.base.sistema.usuario_rol.UsuarioRolWS.class);
               
            

    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.producto;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class ProductoDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public ProductoDAO ( ) throws IOException  {
    }
      
    
    public List<Producto>  list (Integer page) {
                
        List<Producto>  lista = null;        
        try {                        
                        
            ProductoRS rs = new ProductoRS();            
            
            
            lista = new Coleccion<Producto>().resultsetToList(
                    new Producto(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<Producto>  search (Integer page, String busqueda) {
                
        List<Producto>  lista = null;
        
        try {                       
                        
            ProductoRS rs = new ProductoRS();
            lista = new Coleccion<Producto>().resultsetToList(
                    new Producto(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    

    
}

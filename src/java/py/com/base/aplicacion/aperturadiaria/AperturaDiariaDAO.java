/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.aperturadiaria;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;
import py.com.base.aplicacion.aperturadiariadetalle.AperturaDiariaDetalleDAO;


/**
 *
 * @author hugom_000
 */

public class AperturaDiariaDAO  {
        
    /*
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;  
        Lista lista ;
        Integer lineas = Integer.parseInt(new Global().getValue("lineasLista"));
    */ 
        
    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    private Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
    
        
    
    public AperturaDiariaDAO ( ) throws IOException  {

    }
       
       
    
    public AperturaDiaria existe(String strFecha) throws Exception {      

          AperturaDiaria obj = new AperturaDiaria();  

          String sql = 
                    " SELECT apertura, fecha\n" +
                    "  FROM aplicacion.apertura_diaria\n" +
                    "  where fecha =  '"+strFecha+"'" ;  
                
          obj = (AperturaDiaria) persistencia.sqlToObject(sql, obj);

          return obj;          

      }
     


    
    public AperturaDiaria add (String strFecha) throws Exception {      

          AperturaDiaria obj = new AperturaDiaria();  

          String sql = 
                " INSERT INTO aplicacion.apertura_diaria\n" +
                " (fecha)\n" +
                " VALUES('"+strFecha+"')"
                  + " RETURNING apertura ; ";
          
          obj = (AperturaDiaria) persistencia.sqlToObject(sql, obj);

          return obj;          

      }
     

    
    public AperturaDiaria inicioapertura (String strFecha) throws Exception {      

        AperturaDiaria obj = new AperturaDiaria();  

        AperturaDiariaDAO dao = new AperturaDiariaDAO();
        obj = dao.add( strFecha ) ;
    
        // insertaaa lote

            AperturaDiariaDetalleDAO addao = new AperturaDiariaDetalleDAO();
            addao.agregarlotefecha(obj.getApertura());        
        
          
        return obj;          

      }
     


       

      
    
        
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.aperturadiariadetalle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Map;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.RegistroMap;


public class AperturaDiariaDetalleJSON  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public AperturaDiariaDetalleJSON ( ) throws IOException  {
    
    }
      
    
    
    
        
    
    

    public JsonObject  listan ( Integer apertura ) {
        
        Map<String, String> map = null;
        RegistroMap registoMap = new RegistroMap();     
        //Gson gson = new Gson();       
        Gson gson = new GsonBuilder().serializeNulls().create();

        
        
        JsonObject jsonObject = new JsonObject();
        
        try 
        {   
            
            AperturaDiariaDetalleRS rs = new AperturaDiariaDetalleRS();         
            
            ResultSet rsData = rs.listan_data( apertura );                
            
            JsonArray jsonarrayData = new JsonArray();
            while(rsData.next()) 
            {  
                map = registoMap.convertirHashMap(rsData);     
                
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);     

                jsonarrayData.add( element );
            }                    
            //this.total_registros = rs.total_registros  ;   
            
            
            ResultSet rsSuma = rs.listan_sum( apertura );                
            
            
            JsonArray jsonarraySuma = new JsonArray();
            while(rsSuma.next()) 
            {  
                
                map = registoMap.convertirHashMap(rsSuma);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonarraySuma.add( element );

            }                    
            
            jsonObject.addProperty("status", true);
            jsonObject.addProperty("message", "");
            
            jsonObject.add("data", jsonarrayData);    
            jsonObject.add("summary", jsonarraySuma);
    


        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
        
    
    
    
        
}

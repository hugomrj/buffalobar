/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.aperturadiariadetalle;

import java.util.Date;
import py.com.base.aplicacion.producto.Producto;


/**
 *
 * @author hugom_000
 */
public class AperturaDiariaDetalle {
    
    private Integer id;
    private Producto producto;
    private Long precio_venta;
    private Integer apertura_cantidad;
    private Integer cierre_cantidad;
    private Integer venta_cantidad;
    private Long monto_ingreso;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Long getPrecio_venta() {
        return precio_venta;
    }

    public void setPrecio_venta(Long precio_venta) {
        this.precio_venta = precio_venta;
    }

    public Integer getApertura_cantidad() {
        return apertura_cantidad;
    }

    public void setApertura_cantidad(Integer apertura_cantidad) {
        this.apertura_cantidad = apertura_cantidad;
    }

    public Integer getCierre_cantidad() {
        return cierre_cantidad;
    }

    public void setCierre_cantidad(Integer cierre_cantidad) {
        this.cierre_cantidad = cierre_cantidad;
    }

    public Integer getVenta_cantidad() {
        return venta_cantidad;
    }

    public void setVenta_cantidad(Integer venta_cantidad) {
        this.venta_cantidad = venta_cantidad;
    }

    public Long getMonto_ingreso() {
        return monto_ingreso;
    }

    public void setMonto_ingreso(Long monto_ingreso) {
        this.monto_ingreso = monto_ingreso;
    }
    
}

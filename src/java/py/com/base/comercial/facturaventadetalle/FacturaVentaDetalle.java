
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.comercial.facturaventadetalle;

import py.com.base.aplicacion.producto.Producto;




/**
 *
 * @author hugo
 */


public class FacturaVentaDetalle {
    
    
    private Integer venta_detalle;
    private Integer factura;
    private Producto producto;
    private String descripcion;
    private Float cantidad;
    private Long precio ;
    private Long sub_total ;
    private Long gravada0 ;
    private Long gravada5 ;
    private Long gravada10 ;
    
    
    public Integer getVenta_detalle() {
        return venta_detalle;
    }

    public void setVenta_detalle(Integer venta_detalle) {
        this.venta_detalle = venta_detalle;
    }

    public Integer getFactura() {
        return factura;
    }

    public void setFactura(Integer factura) {
        this.factura = factura;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Float getCantidad() {
        return cantidad;
    }

    public void setCantidad(Float cantidad) {
        this.cantidad = cantidad;
    }

    public Long getSub_total() {
        return sub_total;
    }

    public void setSub_total(Long sub_total) {
        this.sub_total = sub_total;
    }

    public Long getGravada0() {
        return gravada0;
    }

    public void setGravada0(Long gravada0) {
        this.gravada0 = gravada0;
    }

    public Long getGravada5() {
        return gravada5;
    }

    public void setGravada5(Long gravada5) {
        this.gravada5 = gravada5;
    }

    public Long getGravada10() {
        return gravada10;
    }

    public void setGravada10(Long gravada10) {
        this.gravada10 = gravada10;
    }



    public Long getPrecio() {
        return precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    

    
}




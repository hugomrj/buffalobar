/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.util.List;
import static javax.ws.rs.client.Entity.json;
import nebuleuse.ORM.Persistencia;
import py.com.base.aplicacion.aperturadiaria.AperturaDiaria;
import py.com.base.aplicacion.aperturadiaria.AperturaDiariaDAO;
import py.com.base.aplicacion.aperturadiariadetalle.AperturaDiariaDetalle;
import py.com.base.aplicacion.aperturadiariadetalle.AperturaDiariaDetalleDAO;
import py.com.base.aplicacion.aperturadiariadetalle.AperturaDiariaDetalleJSON;
import py.com.base.aplicacion.aperturadiariadetalle.AperturaDiariaDetalleRS;
import py.com.base.comercial.cliente.Cliente;
import py.com.base.comercial.cliente.ClienteDAO;
import py.com.base.comercial.facturaventa.FacturaVenta;
import py.com.base.comercial.facturaventa.FacturaVentaDAO;
import py.com.base.comercial.facturaventadetalle.FacturaVentaDetalle;

/**
 *
 * @author hugo
 */
public class Facturaventa_test {


    
    public static void main(String args[]) {
    
        
        Gson gson = new Gson();  


String json = "{\n" +
"  \"boleta\": {\n" +
"    \"boleta\": 0\n" +
"  },\n" +
"  \"factura\": 0,\n" +
"  \"factura_numero\": \"\",\n" +
"  \"fecha_factura\": \"2020-12-02\",\n" +
"  \"cliente\": {\n" +
"    \"cliente\": 0\n" +
"  },\n" +
"  \"cliente_idenficacador\": \"0\",\n" +
"  \"cliente_descrip\": \"Sin nombre\",\n" +
"  \"forma_pago\": {\n" +
"    \"forma_pago\": \"CO\"\n" +
"  },\n" +
"  \"importe_pagado\": 22000,\n" +
"  \"detalle\": [\n" +
"    {\n" +
"      \"venta_detalle\": 0,\n" +
"      \"gravada0\": 0,\n" +
"      \"gravada5\": 0,\n" +
"      \"gravada10\": 15000,\n" +
"      \"factura\": 0,\n" +
"      \"producto\": {\n" +
"        \"producto\": 1\n" +
"      },\n" +
"      \"descripcion\": \"Botella Corona\",\n" +
"      \"cantidad\": 1,\n" +
"      \"precio\": 15000,\n" +
"      \"sub_total\": 15000\n" +
"    }\n" +
"  ]\n" +
"}";        
        



        try {
            
                FacturaVenta facturaventa = new FacturaVenta();      
                Persistencia persistencia = new Persistencia();   

                
                Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
                FacturaVenta req = gsonf.fromJson(json, FacturaVenta.class);              

                                

System.out.println(req.getImporte_pagado());


        

                req.setUsuario( 1 );
                facturaventa = (FacturaVenta) persistencia.insert(req);                                             

                
System.out.println(facturaventa.getImporte_pagado());                



                List<FacturaVentaDetalle> det = req.getDetalle();   
                for (FacturaVentaDetalle fd : det) {                         
                    fd.setFactura(facturaventa.getFactura() );
                    persistencia.insert(fd);   
                }
        
                                
                
                FacturaVentaDAO dao = new FacturaVentaDAO();        
                FacturaVenta f = dao.update_monto( facturaventa.getFactura() );     
                
            
                      
            
            
            
            
        } catch (Exception ex) {
            System.out.println( ex.getCause() );
            System.err.println(ex.getCause());
        }
        
        
    }

        
        

}

